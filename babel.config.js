module.exports = {
  presets: [
    'next/babel',
    ['@babel/preset-env', { targets: { node: true } }],
    '@babel/preset-typescript',
  ],
  plugins: [
    [
      'babel-plugin-styled-components',
      {
        ssr: true,
        minify: true,
        transpileTemplateLiterals: true,
        pure: true,
        displayName: true,
        preprocess: false,
      },
    ],
  ],
};
