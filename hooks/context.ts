import { createContext, useContext } from 'react';
import firebase from 'firebase';

const FirebaseContext = createContext<{
  user: firebase.User,
}>(null);

const useFirebase = () => useContext(FirebaseContext);

export {
  FirebaseContext,
  useFirebase,
};
