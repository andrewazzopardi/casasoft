# Casasoft

## Usage

This application is already available at https://casasoft-techtest.vercel.app/

## Installation

- `npm install`
- `npm run dev`

## Testing - Requires Installation

test coverage is also available at https://andrewazzopardi20.gitlab.io/casasoft/

- `npm run test`