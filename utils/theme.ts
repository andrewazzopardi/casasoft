const theme = {
  colors: {
    white: '#FFFFFF',
    black: '#000000',
    gold: '#FFB546',
  },
  gradients: {
    gold: '#F9E292 0%, #FAD73D 100%',
    sidebarGold: '#FAD136 0%, #FADD49 100%',
    cardDarkener: 'rgba(0, 0, 0, 0) 30.21%, rgba(0, 0, 0, 0.518634) 74.68%, #000000 100%',
  },
  breakpoints: {
    sm: 320,
    md: 640,
    lg: 1280,
  },
};

export type TTheme = typeof theme;
export default theme;
