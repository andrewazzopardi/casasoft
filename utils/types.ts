import React from 'react';

export interface ISlide {
  backgroundUrl: string;
  imageUrl: string;
  title: React.ReactElement;
  description: React.ReactElement;
  actionButton: {
    text: string;
    callback: () => void;
  };
}

export interface IMenuItem {
  imageUrl: string;
  text: string;
}

export interface IPromotion {
  title: string;
  description: string;
  longText: React.ReactNode;
  paperBackgroundUrl: string;
  modalBackgroundUrl: string;
}

export interface ILocation {
  lng: number;
  lat: number;
}

export interface IEvent {
  _id?: string;
  name: string;
  date: string;
  location: ILocation;
}
