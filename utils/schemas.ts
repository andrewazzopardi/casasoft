import * as yup from 'yup';

const createUpdateEventSchema = yup.object({
  name: yup.string().required(),
  date: yup.string().required(),
  location: yup.object({
    lng: yup.number().required(),
    lat: yup.number().required(),
  }).required(),
});

export { createUpdateEventSchema };
