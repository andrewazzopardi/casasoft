import mongoose, { Schema } from 'mongoose';
import { IEvent } from '../../utils/types';

const eventSchema = new Schema({
  name: { type: String, required: true },
  date: { type: Date, required: true },
  location: {
    type: {
      lat: Number,
      lng: Number,
    }, required: true
  },
});

const EventModel = mongoose.models.Event || mongoose.model<IEvent>('Event', eventSchema);

export default EventModel;
