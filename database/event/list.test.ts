import { MONGODB_URI } from '../../testUtils/testDBConnect';
import listEvents from './list';

describe('listEvents', () => {
  process.env.MONGODB_URI = MONGODB_URI;
  it('should give a list of events', async () => {
    const list = await listEvents()
    expect(Array.isArray(list)).toBe(true);
  });
});
