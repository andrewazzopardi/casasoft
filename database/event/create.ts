import dbConnect from '../../utils/dbConnect';
import { IEvent } from '../../utils/types';
import EventModel from '../models/event';

const createEvent = async (event: Omit<IEvent, '_id'>) => {
  await dbConnect();
  const newEvent = new EventModel(event);
  try {
    await newEvent.validate();
  } catch (e) {
    throw new Error('Validation error');
  }
  try {
    await newEvent.save();
    return newEvent;
  } catch (e) {
    throw new Error('Error while creating event');
  }
};

export default createEvent;
