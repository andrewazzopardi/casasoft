import { MONGODB_URI } from '../../testUtils/testDBConnect';
import { IEvent } from '../../utils/types';
import createEvent from './create';

describe('createEvent', () => {
  process.env.MONGODB_URI = MONGODB_URI;
  it('should be successful with good input', async () => {
    const newEvent = await createEvent({
      name: 'goodTest',
      date: '2021-09-11',
      location: {
        lat: 0,
        lng: 0,
      }
    });
    expect(newEvent.name).toBe('goodTest')
  });

  it('should not be successful with bad input', () => {
    expect(async () => {
      await createEvent({
        name: 'badtest',
        date: '2021-09-11',
      } as IEvent);
    }).toThrow()
  });
});
