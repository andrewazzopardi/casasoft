import dbConnect from '../../utils/dbConnect';
import EventModel from '../models/event';

const listEvents = async () => {
  await dbConnect();
  return EventModel.find().exec();
};

export default listEvents;
