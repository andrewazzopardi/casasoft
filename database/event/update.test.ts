import { MONGODB_URI } from '../../testUtils/testDBConnect';
import { IEvent } from '../../utils/types';
import updateEvent from './update';

describe('updateEvent', () => {
  process.env.MONGODB_URI = MONGODB_URI;
  it('should be successful with good input', async () => {
    const newEvent = await updateEvent({
      _id: '612a4b22a87c9a4acbfec1b7',
      name: 'updatedTest',
      date: '2021-09-11',
      location: {
        lat: 0,
        lng: 0,
      }
    });
    expect(newEvent.name).toBe('updatedTest')
  });

  it('should not be successful with bad input', () => {
    expect(async () => {
      await updateEvent({
        _id: '',
        name: 'badtest',
        date: '2021-09-11',
      } as IEvent)
    }).toThrow()
  });
});
