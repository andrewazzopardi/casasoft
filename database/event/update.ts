import dbConnect from '../../utils/dbConnect';
import { IEvent } from '../../utils/types';
import EventModel from '../models/event';

const updateEvent = async (event: IEvent) => {
  await dbConnect();
  const eventToUpdate = await EventModel.findById(event._id).exec();
  if (!eventToUpdate) {
    throw new Error('Error eventToUpdate not found');
  }
  eventToUpdate.name = event.name;
  eventToUpdate.date = event.date;
  eventToUpdate.location = event.location;
  try {
    await EventModel.updateOne({ _id: eventToUpdate._id }, event).exec();
  } catch (e) {
    throw new Error('Error while updating event');
  }
  return eventToUpdate;
};

export default updateEvent;
