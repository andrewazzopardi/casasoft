import styled from 'styled-components';

interface ISolidButtonProps {
  alwaysBig?: boolean;
}

const SolidButton = styled.div<ISolidButtonProps>`
  display: grid;
  place-items: center;
  background: linear-gradient(90deg, ${(props) => props.theme.gradients.gold});
  color: black;
  font-weight: 800;
  ${(props) => (props.alwaysBig ? `
    width: 144px;
    height: 41px;
    font-size: 16px;
    border-radius: 35px;
    ` : `
    width: 81px;
    height: 23px;
    font-size: 9px;
    line-height: 13px;
    border-radius: 19px;
  `)}
  transition: box-shadow 300ms;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    width: 144px;
    height: 41px;
    border-radius: 35px;
    font-size: 16px;
    line-height: 23px;
  }

  &:hover {
    cursor: pointer;
    box-shadow: 0 0 4px 0 ${(props) => props.theme.colors.gold};
  }
`;

export default SolidButton;
