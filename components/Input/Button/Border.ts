import styled from 'styled-components';
import SolidButton from './Solid';

const BorderButton = styled(SolidButton)`
  background: transparent;
  border: 1px solid ${(props) => props.theme.colors.white};
  color: ${(props) => props.theme.colors.white};
  cursor: pointer;

  &:hover {
    background: linear-gradient(90deg, ${(props) => props.theme.gradients.gold});
    border: none;
    color: ${(props) => props.theme.colors.black};
  }
`;

export default BorderButton;
