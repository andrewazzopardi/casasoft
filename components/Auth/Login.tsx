import React, { useEffect } from 'react';
import * as firebaseui from 'firebaseui';
import 'firebaseui/dist/firebaseui.css';
import firebase from 'firebase';
import styled from 'styled-components';

const Container = styled.div`
  display: grid;
  place-items: center;
  height: 100vh;

  & > div {
    min-width: 320px; 
  }
`;

function Login() {
  useEffect(() => {
    const ui = new firebaseui.auth.AuthUI(firebase.auth());
    ui.start('#firebaseui-auth-container', {
      signInOptions: [
        {
          provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
          requireDisplayName: false,
        },
      ],
      callbacks: {
        signInSuccessWithAuthResult: () => false,
      },

    });
  }, []);

  return (
    <Container>
      <div id="firebaseui-auth-container" />
    </Container>
  );
}

export default Login;
