import styled from 'styled-components';
import Map from './Map';

const CenteredMarkedMap = styled(Map)`
  position: relative;
  height: 250px;
  width: 100%;
  
  ::after {
    width: 22px;
    height: 40px;
    display: block;
    content: ' ';
    position: absolute;
    top: 50%; left: 50%;
    margin: -40px 0 0 -11px;
    background: url('https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi_hdpi.png');
    background-size: 22px 40px; /* Since I used the HiDPI marker version this compensates for the 2x size */
    pointer-events: none; /* This disables clicks on the marker. Not fully supported by all major browsers, though */
  }
`;

export default CenteredMarkedMap;
