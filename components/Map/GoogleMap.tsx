import React from 'react';
import GoogleMapReact from 'google-map-react';

interface IGoogleMapProps extends GoogleMapReact.Props {
  children: React.ReactNode;
}

function GoogleMap({ children, ...rest }: IGoogleMapProps) {
  return (
    <GoogleMapReact
      bootstrapURLKeys={{ key: 'AIzaSyBJmGxDPYwYqsyemyhQUxTxwvRtStZJg0k' }}
      yesIWantToUseGoogleMapApiInternals
      defaultZoom={13}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
    >
      {children}
    </GoogleMapReact>
  );
}

export default GoogleMap;
