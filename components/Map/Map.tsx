import googleMapReact from 'google-map-react';
import React from 'react';
import styled from 'styled-components';
import GoogleMap from './GoogleMap';

const Container = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

interface MapProps extends googleMapReact.Props {
  className?: any;
  children?: any;
}

function Map({
  className, children, ...rest
}: MapProps) {
  return (
    <Container className={className}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <GoogleMap {...rest}>
        {children}
      </GoogleMap>
    </Container>
  );
}

export default Map;
