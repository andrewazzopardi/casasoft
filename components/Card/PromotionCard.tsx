import React from 'react';
import styled from 'styled-components';
import { IPromotion } from '../../utils/types';
import BorderButton from '../Input/Button/Border';
import Card from './Card';

const StyledCard = styled(Card)`
  & > div {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
  }
`;

interface IContentProps {
  backgroundUrl: string;
}

const Content = styled.div<IContentProps>`
  display: flex;
  flex-direction: column;
  height: 100%;
  color: ${(props) => props.theme.colors.white};
  padding: 23px 22px;
  background: linear-gradient(180deg, ${(props) => props.theme.gradients.cardDarkener}), url(${(props) => props.backgroundUrl});
  background-size: 120%;
  background-position: center;
  transition: background-size 500ms, transform 500ms;
  border-radius: 10px;
  box-shadow: 3px 4px 8px 0 rgba(0, 0, 0, 0.35);

  &:hover {
    background-size: 105%;
    transform: scale(1.05);
  }
`;

const Spacer = styled.div`
  flex: 1;
`;

const Title = styled.div`
  margin-bottom: 5px;
  color: ${(props) => props.theme.colors.gold};
  font-weight: 800;
  font-size: 13px;
`;

const Description = styled.div`
  margin-bottom: 10px;
  font-size: 13px;
`;

interface IPromotionCardProps {
  promotion: IPromotion;
  onMoreInfo: () => void;
}

function PromotionCard({ promotion, onMoreInfo }: IPromotionCardProps) {
  return (
    <StyledCard>
      <Content backgroundUrl={promotion.paperBackgroundUrl}>
        <Spacer />
        <Title>
          {promotion.title}
        </Title>
        <Description>
          {promotion.description}
        </Description>
        <BorderButton onClick={onMoreInfo}>
          MORE INFO
        </BorderButton>
      </Content>
    </StyledCard>
  );
}

export default PromotionCard;
