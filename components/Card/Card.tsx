import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  border-radius: 10px;
  padding-bottom: 100%;
  position: relative;
`;

const Content = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

interface ICardProps {
  children: React.ReactNode;
  className?: string;
}

function Card({ children, className }: ICardProps) {
  return (
    <Container className={className}>
      <Content>
        {children}
      </Content>
    </Container>
  );
}

export default Card;
