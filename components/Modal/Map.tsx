import React from 'react';
import { ILocation } from '../../utils/types';
import Map from '../Map/Map';
import Marker from '../Map/Marker';
import Modal, { IModalProps } from './Modal';

interface IMapModalProps extends Omit<IModalProps, 'children'> {
  location: ILocation
}

function MapModal({ location, onClose }: IMapModalProps) {
  return (
    <Modal onClose={onClose}>
      <Map
        defaultCenter={location}
        defaultZoom={10}
      >
        <Marker lat={location.lat} lng={location.lng} />
      </Map>
    </Modal>
  );
}

export default MapModal;
