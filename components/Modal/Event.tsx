import axios from 'axios';
import { FormikHelpers, useFormik } from 'formik';
import React from 'react';
import { Button, Form } from 'react-bootstrap';
import styled from 'styled-components';
import { createUpdateEventSchema } from '../../utils/schemas';
import { IEvent } from '../../utils/types';
import CenteredMarkedMap from '../Map/CenteredMarkedMap';
import Modal, { IModalProps } from './Modal';

const Container = styled.div`
  padding: 24px;
`;

const SubmitArea = styled(Form.Group)`
  margin-top: 32px;
`;

interface IEventModelProps extends Omit<IModalProps, 'children'> {
  event: IEvent,
  onCreate: (event: IEvent) => void,
}

function EventModal({ event, onClose, onCreate }: IEventModelProps) {
  const onSubmit = (values: IEvent, { setSubmitting }: FormikHelpers<any>) => {
    setSubmitting(true);
    axios.post('/api/events', values).then((response) => {
      onCreate(response.data);
      setSubmitting(false);
      onClose();
    });
  };

  const {
    values, handleChange, handleBlur, errors, touched, handleSubmit, setFieldValue,
  } = useFormik({
    onSubmit,
    initialValues: event,
    validationSchema: createUpdateEventSchema,
  });

  return (
    <Modal onClose={onClose}>
      <Container>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control id="name" onChange={handleChange} onBlur={handleBlur} value={values.name} type="text" />
            {touched.name && errors.name && <Form.Text>{errors.name}</Form.Text>}
          </Form.Group>
          <Form.Group>
            <Form.Label>Date</Form.Label>
            <Form.Control id="date" onChange={handleChange} onBlur={handleBlur} value={values.date.split('T')[0]} type="date" />
            {touched.date && errors.date && <Form.Text>{errors.date}</Form.Text>}
          </Form.Group>
          <Form.Group>
            <Form.Label>Location</Form.Label>
            <CenteredMarkedMap
              defaultCenter={values.location}
              defaultZoom={0}
              onChange={(value) => setFieldValue('location', value.center)}
            />
            {touched.location && errors.location && <Form.Text>{errors.location}</Form.Text>}
          </Form.Group>
          <SubmitArea>
            <Button type="submit">{event._id ? 'Edit' : 'Create'}</Button>
          </SubmitArea>
        </Form>
      </Container>
    </Modal>
  );
}

export default EventModal;
