import React from 'react';
import styled from 'styled-components';
import { IPromotion } from '../../utils/types';
import SolidButton from '../Input/Button/Solid';
import Modal, { IModalProps } from './Modal';

interface IHeaderProps {
  backgroundUrl: string;
}

const Header = styled.div<IHeaderProps>`
  background: linear-gradient(180deg, ${(props) => props.theme.gradients.cardDarkener}), url(${(props) => props.backgroundUrl});
  display: grid;
  grid-gap: 8px 24px;
  grid-template-rows: 280px auto auto;
  grid-template-areas: 
    '.'
    'title'
    'description'
    'activate';
  padding: 32px;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    grid-gap: 0 24px;
    grid-template-areas: 
      '. .'
      'title activate'
      'description activate';
  }
`;

const Title = styled.div`
  grid-area: title;
  font-weight: 800;
  font-size: 25.0127px;
  color: ${(props) => props.theme.colors.gold};
`;

const Description = styled.div`
  grid-area: description;
  color: ${(props) => props.theme.colors.white};
`;

const StyledSolidButton = styled(SolidButton)`
  grid-area: activate;
  
  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    place-self: center;
  }
`;

const Content = styled.div`
  padding: 42px 50px;
`;

interface IPromotionModalProps extends Omit<IModalProps, 'children'> {
  promotion: IPromotion;
}

function PromotionModal({ onClose, promotion }: IPromotionModalProps) {
  return (
    <Modal onClose={onClose}>
      <Header backgroundUrl={promotion.modalBackgroundUrl}>
        <Title>{promotion.title}</Title>
        <Description>{promotion.description}</Description>
        <StyledSolidButton alwaysBig>ACTIVATE</StyledSolidButton>
      </Header>
      <Content>
        {promotion.longText}
      </Content>
    </Modal>
  );
}

export default PromotionModal;
