import React, { useEffect } from 'react';
import ReactModal from 'react-modal';
import styled from 'styled-components';

const StyledReactModal = styled(ReactModal)`
  background: white;
  position: absolute;
  overflow: auto;
  outline: none;
  padding: 0px;
  border: 0px;
  inset: 0;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    inset: 40px;
    border-radius: 4px;
  }
`;

const ContentWrapper = styled.div`
  position: relative;
  height: 100%;
`;

const Content = styled.div`
  height: 100%;
  overflow: auto;
`;

const CloseButtonContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  background: black; 
  background-image: url(/images/close.svg) no-repeat;
  background-position: center;
  width: 33px;
  height: 33px;
  cursor: pointer;
  filter: invert(1);

  &:hover {
    filter: invert(0);
  }
`;

const CloseButton = styled.div`
  background: url(/images/close.svg) no-repeat;
  background-position: center;
  width: 33px;
  height: 33px;
  transform: scale(0.7);
  filter: invert(0);
`;

export interface IModalProps {
  children: React.ReactNode;
  onClose: () => void;
}

function Modal({ children, onClose }: IModalProps) {
  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  return (
    <StyledReactModal
      isOpen
      shouldCloseOnEsc
      shouldCloseOnOverlayClick
      onRequestClose={onClose}
      style={{
        overlay: {
          zIndex: 3,
          background: 'rgba(0, 0, 0, 0.6)',
        },
      }}
    >
      <ContentWrapper>
        <Content>
          {children}
        </Content>
        <CloseButtonContainer>
          <CloseButton onClick={onClose} />
        </CloseButtonContainer>
      </ContentWrapper>
    </StyledReactModal>
  );
}

export default Modal;
