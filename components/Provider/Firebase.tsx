import React, { useEffect, useState } from 'react';
import firebase from 'firebase';
import { FirebaseContext } from '../../hooks/context';

const firebaseConfig = {
  apiKey: 'AIzaSyDDVLo2oD1zaFnm37yvIqmzWr9I8eod1A4',
  authDomain: 'casasoft-techtest.firebaseapp.com',
  projectId: 'casasoft-techtest',
  storageBucket: 'casasoft-techtest.appspot.com',
  messagingSenderId: '819772913921',
  appId: '1:819772913921:web:c8628a75a59bacd8d04300',
  measurementId: 'G-ZCH5EFZVD5',
};

interface IFirebaseProviderProps {
  children: React.ReactNode;
}

function FirebaseProvider({ children }: IFirebaseProviderProps) {
  const [user, setUser] = useState(null);

  useEffect(() => {
    let app: firebase.app.App;
    if (firebase.apps.length === 0) {
      app = firebase.initializeApp(firebaseConfig);
    } else {
      [app] = firebase.apps;
    }
    app.auth().onAuthStateChanged((currentUser: firebase.User) => {
      setUser(currentUser);
    });
  }, []);

  return (
    <FirebaseContext.Provider value={{
      user,
    }}
    >
      {children}
    </FirebaseContext.Provider>
  );
}

export default FirebaseProvider;
