import React from 'react';
import styled from 'styled-components';
import { ISlide } from '../../utils/types';
import SolidButton from '../Input/Button/Solid';

interface IContainerProps {
  backgroundUrl: string;
}

const Container = styled.div<IContainerProps>`
  height: 353px;
  background: url(${(props) => props.backgroundUrl});
  background-size: cover;
  padding: 53px 17px;
  display: flex;
  justify-content: space-between;
  align-items: start;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    padding: 110px 96px;
    height: 500px;
  }
`;

const InfoBox = styled.div`
  background: rgba(255,255,255,0.6);
  color: ${(props) => props.theme.colors.black};
  z-index: 1;

  padding: 26px 30px;
  max-width: 344px;
  border-radius: 11px;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    padding: 26px 30px;
    max-width: 613px;
    border-radius: 11px;
  }
`;

const Title = styled.div`
  margin-bottom: 2.25px;
  font-weight: bold;
  font-size: 18px;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    margin-bottom: 4px;
    font-weight: bold;
    font-size: 32px;
    line-height: 37px;
  }
`;

const Description = styled.div`
  font-weight: normal;
  font-size: 10px;
  margin-bottom: 8px;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    margin-bottom: 15px;
    font-size: 18px;
    line-height: 25px;
  }
`;

const ImageContainer = styled.div`
  position: relative;
  flex: 0 0 auto;
  z-index: 0;
  align-self: center;
  
  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    align-self: start;
  }
`;

interface IImageProps {
  imageUrl: string;
}

const ActualImage = styled.div<IImageProps>`
  background: url(${(props) => props.imageUrl}) no-repeat;;
  background-size: contain;
  background-position: bottom;
  width: 134px;
  height: 159px;
  position: absolute;
  top: 100%;
  right: 0;

  @media (min-width: ${(props) => props.theme.breakpoints.md}px) {
    width: 313px;
    height: 373px;
  }
`;

interface IItemProps {
  slide: ISlide;
}

function CarouselItem({ slide }: IItemProps) {
  return (
    <Container backgroundUrl={slide.backgroundUrl}>
      <InfoBox>
        <Title>
          {slide.title}
        </Title>
        <Description>
          {slide.description}
        </Description>
        <SolidButton onClick={slide.actionButton.callback}>
          {slide.actionButton.text}
        </SolidButton>
      </InfoBox>
      <ImageContainer>
        <ActualImage imageUrl={slide.imageUrl} />
      </ImageContainer>
    </Container>
  );
}

export default CarouselItem;
