import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styled from 'styled-components';
import CarouselItem from './Item';
import { ISlide } from '../../utils/types';

const slides: ISlide[] = [{
  backgroundUrl: '/images/back-1.png',
  imageUrl: '/images/image-1.png',
  title: (
    <span>
      Grab 100% up to €200
      <br />
      {' '}
      &amp; 200 Free Spins
    </span>
  ),
  description: (
    <span>
      Get ready to party with us!
      {' '}
      <b>Unlock hundreds in bonus cash</b>
      {' '}
      and collect piles of
      {' '}
      <b>free spins for 10 days</b>
      {' '}
      on your first deposit of €20 or more.
    </span>
  ),
  actionButton: {
    text: 'SIGN UP',
    callback: () => { },
  },
}, {
  backgroundUrl: '/images/back-2.jpg',
  imageUrl: '/images/image-2.png',
  title: (
    <span>
      Win and enjoy!
    </span>
  ),
  description: (
    <span>
      Get the most of your wins and enjoy your holiday!
    </span>
  ),
  actionButton: {
    text: 'SIGN UP',
    callback: () => { },
  },
}, {
  backgroundUrl: '/images/back-3.png',
  imageUrl: '/images/image-3.png',
  title: (
    <span>
      Ready for the thrill?
    </span>
  ),
  description: (
    <span>
      Isn&apos;t it exciting to win whilst you&apos;re enjoying from home.
      Then come visit our portal and get your chance to win unlimited
      prizes that can change your life from today to tomorrow!  Are you ready?
    </span>
  ),
  actionButton: {
    text: 'SIGN UP',
    callback: () => { },
  },
}];

const Container = styled.div`
  overflow: hidden;
`;

const StyledSlider = styled(Slider)`
  & > .slick-dots {
    bottom: 48px;

    & > li {
      width: 10px;
      height: 10px;
      margin: 0 10px;

      & > button {
        width: 8px;
        height: 8px;
        border-radius: 5px;
        border: 1px solid #C4C4C4;
  
        &::before {
          content: unset;
        }
      }
  
      &.slick-active > button {
        background: #C4C4C4;
      }
    }
  }
`;

function Carousel() {
  return (
    <Container>
      <StyledSlider
        dots
        infinite
        speed={500}
        slidesToShow={1}
        slidesToScroll={1}
      >
        {slides.map((slide, index) => <CarouselItem key={index.toString()} slide={slide} />)}
      </StyledSlider>
    </Container>
  );
}

export default Carousel;
