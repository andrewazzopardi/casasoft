import React, { useState } from 'react';
import styled from 'styled-components';
import Sidebar from './Sidebar/Sidebar';

const Container = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  min-height: 100vh;
`;

const Content = styled.div`
  position: relative;
  min-width: 0;
  max-width: 100vw;
`;

interface IContentOverlayProps {
  isVisible: boolean;
}

const ContentOverlay = styled.div<IContentOverlayProps>`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.6);
  ${(props) => (props.isVisible ? `
    z-index: 1;
    opacity: 1;
  ` : `
    z-index: -1;
    opacity: 0;
  `)}
  transition: z-index 300ms ${(props) => (props.isVisible ? 0 : 300)}ms, opacity 300ms;
`;

interface ILayoutProps {
  children: React.ReactNode;
}

function Layout({ children }: ILayoutProps) {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  return (
    <Container>
      <Sidebar
        isOpen={isSidebarOpen}
        setIsOpen={setIsSidebarOpen}
      />
      <Content>
        {children}
        <ContentOverlay isVisible={isSidebarOpen} />
      </Content>
    </Container>
  );
}

export default Layout;
