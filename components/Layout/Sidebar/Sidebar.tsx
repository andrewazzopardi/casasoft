import React from 'react';
import styled from 'styled-components';
import { IMenuItem } from '../../../utils/types';
import SidebarItem from './Item';

const menuItems: IMenuItem[] = [{
  imageUrl: '/images/off-canvas-homepage.svg',
  text: 'Homepage',
}, {
  imageUrl: '/images/off-canvas-live-casino.svg',
  text: 'Live Casino',
}, {
  imageUrl: '/images/off-canvas-gift.svg',
  text: 'Promotions',
}, {
  imageUrl: '/images/off-canvas-headset.svg',
  text: 'Contact',
}];

interface IOpenable {
  isOpen: boolean;
}

const Container = styled.div<IOpenable>`
  position: sticky;
  top: 0;
  left: 0;
  height: 100vh;
  width: min(calc(100vw - 90px), ${(props) => (props.isOpen ? 352 : 0)}px);
  transition: width 300ms;
  background: linear-gradient(90deg, ${(props) => props.theme.gradients.gold});
  z-index: 2;
`;

const Content = styled.div`
  width: 100%;
  padding: 23px 0;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: start;
`;

const ToggleButton = styled.div<IOpenable>`
  position: absolute;
  top: 26px;
  left: calc(100% + 29px);
  width: 21px;
  height: 23px;
  cursor: pointer;
  background: url('/images/${(props) => (props.isOpen ? 'close' : 'open')}.svg') no-repeat;
  transition: right 300ms;
`;

interface ISidebarProps extends IOpenable {
  setIsOpen: (value: boolean) => void;
}

function Sidebar({ isOpen, setIsOpen }: ISidebarProps) {
  return (
    <Container isOpen={isOpen}>
      <Content>
        {menuItems.map((menuItem, index) => (
          <SidebarItem key={index.toString()} menuItem={menuItem} />
        ))}
      </Content>
      <ToggleButton isOpen={isOpen} onClick={() => setIsOpen(!isOpen)} />
    </Container>
  );
}

export default Sidebar;
