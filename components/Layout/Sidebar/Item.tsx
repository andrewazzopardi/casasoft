import React from 'react';
import styled from 'styled-components';
import { IMenuItem } from '../../../utils/types';

const Container = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  height: 40px;
  margin: 0 0 15px 23px;

  &:hover {
    border-bottom: 3px solid ${(props) => props.theme.colors.gold};
    height: 40px;

    & > div {
      font-weight: 800;
    }
  }
`;

interface IImageProps {
  imageUrl: string;
}

const Image = styled.div<IImageProps>`
  background: url(${(props) => props.imageUrl});
  background-size: contain;
  width: 27px;
  height: 27px;
  margin-right: 15px;
`;

const Text = styled.div`
  font-weight: 600;
  font-size: 16px;
  color: ${(props) => props.theme.colors.black};
`;

interface ISidebarItemProps {
  menuItem: IMenuItem;
}

function SidebarItem({ menuItem }: ISidebarItemProps) {
  return (
    <Container>
      <Image imageUrl={menuItem.imageUrl} />
      <Text>
        {menuItem.text}
      </Text>
    </Container>
  );
}

export default SidebarItem;
