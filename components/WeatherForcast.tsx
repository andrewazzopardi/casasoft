import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { ILocation } from '../utils/types';

interface IWeatherForcastProps {
  location: ILocation,
  date: string,
}

function WeatherForcast({ location, date }: IWeatherForcastProps) {
  const [forecast, setForecast] = useState('loading...');

  useEffect(() => {
    const parsedDate = new Date(date);
    const dateUnix = Date.UTC(
      parsedDate.getUTCFullYear(), parsedDate.getUTCMonth(), parsedDate.getUTCDay(),
    );
    const diff = dateUnix - Date.now();
    const diffInDays = Math.floor(diff / (1000 * 60 * 60 * 24));
    axios.get(`https://api.openweathermap.org/data/2.5/forecast/daily?lat=${location.lat}&lon=${location.lng}&cnt=${diffInDays}&appid=${process.env.NEXT_PUBLIC_WEATHER_API_KEY}`).then((response) => {
      setForecast(response.data.list[diffInDays].weather.description);
    }).catch(() => setForecast('Error'));
  }, [date, location.lat, location.lng]);

  return (
    <div>
      {forecast}
    </div>
  );
}

export default WeatherForcast;
