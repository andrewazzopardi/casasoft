import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import Modal from 'react-modal';
import Layout from '../components/Layout/Layout';
import theme from '../utils/theme';
import FirebaseProvider from '../components/Provider/Firebase';

Modal.setAppElement('#__next');

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }

  * {
    font-family: Nunito;
    font-style: normal;
    box-sizing: border-box;
  }
`;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <FirebaseProvider>
        <ThemeProvider theme={theme}>
          <Layout>
            {/* eslint-disable-next-line react/jsx-props-no-spreading */}
            <Component {...pageProps} />
          </Layout>
        </ThemeProvider>
      </FirebaseProvider>
    </>
  );
}

export default MyApp;
