import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';

const Container = styled.div`
  margin-top: 64px;

  & > ol > ol > li {
    cursor: pointer;
  }
`;

function IndexPage() {
  return (
    <Container>
      Click to go to each task
      <ol>
        <li>Frontend</li>
        <ol>
          <Link passHref href="/carousel">
            <li>Carousel</li>
          </Link>
          <Link passHref href="/list">
            <li>List View</li>
          </Link>
        </ol>
        <li>Backend</li>
        <ol>
          <Link passHref href="/login">
            <li>Login</li>
          </Link>
          <Link passHref href="/events">
            <li>Events</li>
          </Link>
        </ol>
      </ol>
    </Container>
  );
}

export default IndexPage;
