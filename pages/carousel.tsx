import React from 'react';
import Carousel from '../components/Carousel/Carousel';

function CarouselPage() {
  return (
    <Carousel />
  );
}

export default CarouselPage;
