import { NextApiRequest, NextApiResponse } from 'next';
import { Asserts } from 'yup';
import createEvent from '../../database/event/create';
import listEvents from '../../database/event/list';
import updateEvent from '../../database/event/update';
import { createUpdateEventSchema } from '../../utils/schemas';
import { IEvent } from '../../utils/types';

interface PostEventInput extends Asserts<typeof createUpdateEventSchema> { }

const handle = async (req: NextApiRequest, res: NextApiResponse) => {
  let data: IEvent | IEvent[];

  switch (req.method) {
    case 'GET': {
      data = await listEvents();
      break;
    }
    case 'POST': {
      const validated: PostEventInput = createUpdateEventSchema.cast(req.body);
      if (req.body._id) {
        data = await updateEvent(validated);
      } else {
        data = await createEvent(validated);
      }
      break;
    }
    default: {
      res.statusCode = 405;
      break;
    }
  }
  res.send(data);
};

export default handle;
