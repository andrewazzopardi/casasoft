import React, { useEffect } from 'react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useFirebase } from '../hooks/context';

const Login = dynamic<unknown>(() => import('../components/Auth/Login'), { ssr: false });

function LoginPage() {
  const router = useRouter();
  const { user } = useFirebase();

  useEffect(() => {
    if (user) {
      router.replace('/events');
    }
  }, [router, user]);

  return (
    <Login />
  );
}

export default LoginPage;
