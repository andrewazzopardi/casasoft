import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Table } from 'react-bootstrap';
import styled from 'styled-components';
import firebase from 'firebase';
import axios from 'axios';
import { useFirebase } from '../hooks/context';
import SolidButton from '../components/Input/Button/Solid';
import EventModal from '../components/Modal/Event';
import { IEvent, ILocation } from '../utils/types';
import MapModal from '../components/Modal/Map';
import WeatherForcast from '../components/WeatherForcast';

const newEvent: IEvent = {
  name: '',
  date: new Date().toISOString().split('T')[0],
  location: {
    lat: 0,
    lng: 0,
  },
};

const Container = styled.div`
  padding-top: 70px;
  min-height: 100vh;
`;

function EventsPage() {
  const router = useRouter();
  const { user } = useFirebase();
  const [eventModalOpen, setEventModalOpen] = useState<IEvent>(null);
  const [locationModalOpen, setLocationModalOpen] = useState<ILocation>(null);
  const [events, setEvents] = useState<Record<string, IEvent>>({});

  useEffect(() => {
    axios.get('/api/events').then((response) => {
      const nextEvents = [...response.data].reduce((p, v) => { p[v._id] = v; return p; }, {});
      setEvents(nextEvents);
    });
  }, []);

  return (
    <Container>
      {user ? (
        <SolidButton alwaysBig onClick={() => firebase.auth().signOut()}>Log out</SolidButton>
      ) : (
        <SolidButton alwaysBig onClick={() => router.push('/login')}>Log in</SolidButton>
      )}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Location</th>
            <th>Expected Weather</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          {Object.values(events).map((event, index) => (
            <tr key={index.toString()}>
              <td>{event?.name}</td>
              <td>{event?.date?.split('T')[0]}</td>
              <td>
                <div
                  role="button"
                  onClick={() => setLocationModalOpen(event.location)}
                  onKeyDown={(e) => e.key === '+' && setLocationModalOpen(event.location)}
                  tabIndex={0}
                >
                  View Location
                </div>
              </td>
              <td>
                {event.location && event.date && <WeatherForcast location={event.location} date={event.date} />}
              </td>
              <td>
                <div
                  role="button"
                  onClick={() => setEventModalOpen(event)}
                  onKeyDown={(e) => e.key === '+' && setEventModalOpen(event)}
                  tabIndex={0}
                >
                  Edit
                </div>
              </td>
            </tr>
          ))}
          {user && (
            <tr>
              <td colSpan={5}>
                <div
                  role="button"
                  onClick={() => setEventModalOpen(newEvent)}
                  onKeyDown={(e) => e.key === '+' && setEventModalOpen(newEvent)}
                  tabIndex={0}
                >
                  Create
                </div>
              </td>
            </tr>
          )}
        </tbody>
      </Table>
      {eventModalOpen && (
        <EventModal
          event={eventModalOpen}
          onClose={() => setEventModalOpen(null)}
          onCreate={(event) => setEvents({ ...events, [event._id]: event })}
        />
      )}
      {locationModalOpen && (
        <MapModal
          location={locationModalOpen}
          onClose={() => setLocationModalOpen(null)}
        />
      )}
    </Container>
  );
}

export default EventsPage;
