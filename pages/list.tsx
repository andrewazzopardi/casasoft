import React, { useState } from 'react';
import styled from 'styled-components';
import PromotionCard from '../components/Card/PromotionCard';
import PromotionModal from '../components/Modal/Promotion';
import { IPromotion } from '../utils/types';

const Container = styled.div`
  padding: 81px 30px;
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(322px, 1fr));
  width: 100%;
  grid-gap: 31px;
`;

const longFakeText = (
  <div>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. In gravida enim diam, et
      scelerisque sem egestas quis. In ac purus bibendum, pulvinar lorem quis, sagittis
      nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at enim
      bibendum odio fermentum semper. Phasellus ipsum dolor, interdum vitae consequat
      in, bibendum sit amet elit. Phasellus scelerisque urna at pharetra blandit.
      Maecenas ultricies, sem vel laoreet accumsan, sem tellus convallis ligula,
      aliquet consequat urna nisi ac erat. Duis non efficitur magna,
      id sagittis leo. Praesent congue, lectus eu mollis mattis, leo sem maximus
      ante, ut rhoncus enim enim id sapien
    </p>
    <p>
      Aenean maximus erat sit amet nulla accumsan, in dictum urna vehicula. Donec finibus
      aliquet dolor, ut interdum mi sodales in. Nulla ut dapibus dolor, non semper dui. Sed
      neque erat, laoreet eu egestas ut, venenatis et arcu. Maecenas dapibus quis orci sed
      tincidunt. Vestibulum tincidunt tristique sapien in malesuada. Nunc aliquam quis lorem
      malesuada vestibulum. Aliquam suscipit iaculis arcu, semper cursus odio dictum eu.
      Donec sed eros id eros ultricies auctor sit amet non magna. Duis at massa vitae erat
      rhoncus mattis. Aliquam ultricies, neque ullamcorper finibus ultricies, neque est
      convallis risus, sit amet commodo dui est molestie massa. Aenean eu sollicitudin ante.
      Fusce luctus augue ipsum, quis sodales nunc blandit at. Vestibulum ante ipsum primis
      in faucibus orci luctus et ultrices posuere cubilia curae; Sed a bibendum augue.

    </p>
    <p>
      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
      Nunc hendrerit, nulla non condimentum feugiat, nisl ligula euismod dui, sit amet fermentum
      erat arcu ut neque. Aliquam tempor congue tortor, vitae hendrerit lacus dignissim
      ullamcorper. Donec scelerisque ac turpis non rhoncus. Integer orci dui, tempor eget
      urna eget, maximus cursus ipsum. Vestibulum ante ipsum primis in faucibus orci luctus
      et ultrices posuere cubilia curae; Etiam cursus tellus sit amet arcu tristique, a
      feugiat tellus volutpat. Interdum et malesuada fames ac ante ipsum primis in
      faucibus. Morbi sed dui volutpat, interdum quam eu, faucibus lacus.

    </p>
    <p>
      Mauris metus justo, tristique vitae imperdiet sed, congue eu felis. Pellentesque habitant
      morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam quis
      semper tortor, in viverra neque. In nec lacinia nulla, sed viverra turpis. Sed tristique
      rhoncus arcu non gravida. Mauris rhoncus consequat justo, nec rutrum augue rutrum vitae.
      Proin sit amet mollis est. In in gravida lacus, id aliquam nunc. Praesent interdum ante
      arcu, sit amet ultricies nisi consequat eget.
    </p>
    <p>
      Nulla maximus suscipit nunc, sollicitudin lacinia dui. Vestibulum pretium at velit non
      molestie. Maecenas scelerisque vitae lorem tristique varius. Etiam at nisl aliquam,
      egestas tortor sit amet, mollis nibh. Donec nec risus non quam pulvinar bibendum
      suscipit quis sem. Nulla ut accumsan est, at laoreet urna. Nunc posuere ut dolor
      ut ornare. In lacinia viverra dui at posuere.
    </p>
  </div>
);

const fakePromotion1: IPromotion = {
  title: 'GET €1000 IN BONUSES & 1000 FREE SPINS',
  description: 'Receive up to €100 for free when making your first deposit & you get 1000 free spins',
  longText: longFakeText,
  paperBackgroundUrl: '/images/promo-1.jpg',
  modalBackgroundUrl: '/images/promo-full-1.jpg',
};
const fakePromotion2: IPromotion = {
  title: 'GET €1000 IN BONUSES & 1000 FREE SPINS',
  description: 'Receive up to €100 for free when making your first deposit & you get 1000 free spins',
  longText: longFakeText,
  paperBackgroundUrl: '/images/promo-2.jpg',
  modalBackgroundUrl: '/images/promo-full-2.jpg',
};
const fakePromotion3: IPromotion = {
  title: 'GET €1000 IN BONUSES & 1000 FREE SPINS',
  description: 'Receive up to €100 for free when making your first deposit & you get 1000 free spins',
  longText: longFakeText,
  paperBackgroundUrl: '/images/promo-3.jpg',
  modalBackgroundUrl: '/images/promo-full-3.jpg',
};

const promotions: IPromotion[] = [
  fakePromotion1,
  fakePromotion2,
  fakePromotion3,
  fakePromotion1,
  fakePromotion2,
  fakePromotion3,
  fakePromotion1,
  fakePromotion2,
  fakePromotion3,
  fakePromotion1,
];

function IndexPage() {
  const [promotionModalOpen, setPromotionModalOpen] = useState<IPromotion>(null);

  return (
    <Container>
      <Grid>
        {promotions.map((promotion, index) => (
          <PromotionCard
            key={index.toString()}
            promotion={promotion}
            onMoreInfo={() => setPromotionModalOpen(promotion)}
          />
        ))}
      </Grid>
      {promotionModalOpen && (
        <PromotionModal
          promotion={promotionModalOpen}
          onClose={() => setPromotionModalOpen(null)}
        />
      )}
    </Container>
  );
}

export default IndexPage;
