module.exports = {
  preset: 'ts-jest',
  collectCoverageFrom: ['**/*.ts', '!**/*.d.ts', '!**/node_modules/**'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  testMatch: ['**/*.test.(ts|tsx)'],
  testPathIgnorePatterns: [
    './.next/',
    './node_modules/',
  ],
};
